class AddFeesAndChargesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :listing_fee, :float
    add_column :products, :payment_charges, :float
    add_column :products, :total, :float
  end
end
