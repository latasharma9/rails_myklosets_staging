class AddSubscriptionToUser < ActiveRecord::Migration
  def change
  	add_column :users, :subscription_id, :integer
    add_column :users, :is_subscribed, :boolean, :default=>false
    add_index :users, :subscription_id
  end
end
