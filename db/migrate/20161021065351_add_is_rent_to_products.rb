class AddIsRentToProducts < ActiveRecord::Migration
  def change
    add_column :products, :is_rent, :boolean, :default=>false
  end
end
