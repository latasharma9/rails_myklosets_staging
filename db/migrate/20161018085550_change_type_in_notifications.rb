class ChangeTypeInNotifications < ActiveRecord::Migration
   def self.up
    rename_column :notifications, :type, :message_type
  end

  def self.down
    rename_column :notifications, :message_type, :type
  end
end
