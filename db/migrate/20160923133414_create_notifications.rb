class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :type
      t.text :message
      t.boolean :status, :default => false
      t.references :user
      t.references :product
      t.timestamps null: false
    end    
  end
end
