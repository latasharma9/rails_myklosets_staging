class AddIsDonatedToProducts < ActiveRecord::Migration
  def change
  	 add_column :products, :is_donated, :boolean, :default=>false
  end
end
