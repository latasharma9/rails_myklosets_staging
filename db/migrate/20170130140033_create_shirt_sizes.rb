class CreateShirtSizes < ActiveRecord::Migration
  def change
    create_table :shirt_sizes do |t|
      t.string :bust

      t.timestamps null: false
    end
  end
end
