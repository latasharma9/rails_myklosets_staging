class AddIsSubAdminToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :is_sub_admin, :boolean
  end
end
