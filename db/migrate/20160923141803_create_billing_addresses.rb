class CreateBillingAddresses < ActiveRecord::Migration
  def change
    create_table :billing_addresses do |t|
      t.string :street_address
      t.string :city
      t.string :state
      t.integer :zip_code
      t.string :mobile
      t.string :phone
      t.references :user
      t.references :country
      t.timestamps null: false
    end
  end
end
