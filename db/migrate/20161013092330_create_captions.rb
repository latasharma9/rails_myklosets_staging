class CreateCaptions < ActiveRecord::Migration
  def change
    create_table :captions do |t|
      t.string :title
      t.references :attachment
      t.timestamps null: false
    end
  end
end
