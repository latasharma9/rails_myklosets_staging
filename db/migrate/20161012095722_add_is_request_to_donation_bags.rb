class AddIsRequestToDonationBags < ActiveRecord::Migration
  def self.up
    add_column :donation_bags, :status, :boolean, default: false
  end

  def self.down
    remove_column :donation_bags, :status
  end
end
