class CreateProductReturns < ActiveRecord::Migration
  def change
    create_table :product_returns do |t|
      t.text :reason
      t.references :order
      t.timestamps null: false
    end    
  end
end
