class ChangeStausDefaultToProducts < ActiveRecord::Migration
    def up
      change_column_default :products, :status, true
    end

    def down
      change_column_default :products, :status, false
    end
end
