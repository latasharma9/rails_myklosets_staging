class AddFieldToSellerBankDetails < ActiveRecord::Migration
  def change
    add_column :seller_bank_details, :save_for_future_use, :boolean, :default => 0
  end
end
