class AddShirtSizeIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :shirt_size_id, :integer
  end
end
