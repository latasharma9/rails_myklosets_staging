class RemoveFieldsFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :weight
    remove_column :products, :units
    remove_column :products, :length
    remove_column :products, :dimension_units
    remove_column :products, :width
    remove_column :products, :height
  end
end