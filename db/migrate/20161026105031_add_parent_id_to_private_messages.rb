class AddParentIdToPrivateMessages < ActiveRecord::Migration
  def change
    add_column :private_messages, :parent_id, :integer, :default => nil
  end
end
