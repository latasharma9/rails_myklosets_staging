class CreateSellerReturnPolicies < ActiveRecord::Migration
  def change
    create_table :seller_return_policies do |t|
      t.boolean :is_allow, :default => true
      t.boolean :is_pay_for_return_policy, :default => true
      t.integer :notify_days, :default => 0,  :null => false
      t.references :user
      t.timestamps null: false
    end
  end
end
