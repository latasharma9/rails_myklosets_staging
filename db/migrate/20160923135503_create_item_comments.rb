class CreateItemComments < ActiveRecord::Migration
  def change
    create_table :item_comments do |t|
      t.text :description
      t.references :user
      t.references :product
      t.timestamps null: false
    end    
  end
end
