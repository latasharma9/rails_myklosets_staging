class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.string :name
      t.float :price
      t.boolean :status

      t.timestamps null: false
    end
  end
end
