class AddMyClosetEarningsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :my_closet_earnings, :string
  end
end
