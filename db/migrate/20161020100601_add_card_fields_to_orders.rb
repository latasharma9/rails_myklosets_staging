class AddCardFieldsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :card_type, :string
    add_column :orders, :card_number, :string
  end
end
