class CreateDonationBags < ActiveRecord::Migration
  def change
    create_table :donation_bags do |t|
      t.boolean :is_receive_email_receipt, :default => false
      t.string :shipping_address
      t.references :user
      t.timestamps null: false
    end    
  end
end
