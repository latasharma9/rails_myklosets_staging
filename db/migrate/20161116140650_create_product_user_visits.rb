class CreateProductUserVisits < ActiveRecord::Migration
  def change
    create_table :product_user_visits do |t|
      t.integer :product_id, :null => false
      t.integer :user_id, :null => false
      t.integer :visits, :default => 0

      t.timestamps null: false
    end
  end
end
