class AddWishlistsCountToProducts < ActiveRecord::Migration
  def change
    add_column :products, :wishlists_count, :integer
  end
end
