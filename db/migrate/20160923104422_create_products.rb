class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :headline
      t.text :description
      t.float :original_price
      t.float :selling_price
      t.references :user
      t.references :size
      t.references :cup
      t.references :condition
      t.references :fabric
      t.references :type
      t.float :weight
      t.string :units
      t.float :length
      t.float :width
      t.float :height
      t.string :dimension_units
      t.boolean :status, :default => false
      t.boolean :is_sold, :default => false

      t.timestamps null: false
    end    
  end
end
