class CreateCashouts < ActiveRecord::Migration
  def change
    create_table :cashouts do |t|
      t.float :amount, default: 0
      t.references :user
      t.boolean :status, default: false
      t.string :transcation_id
      t.timestamps null: false
    end
  end
end
