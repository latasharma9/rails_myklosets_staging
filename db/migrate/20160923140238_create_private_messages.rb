class CreatePrivateMessages < ActiveRecord::Migration
  def change
    create_table :private_messages do |t|
      t.text :message
      t.boolean :is_read, :default => false
      t.integer :receiver_id
      t.references :user
      t.references :product
      t.timestamps null: false
    end    
  end
end
