class AddBagsCountToDonationBags < ActiveRecord::Migration
  def change
    add_column :donation_bags, :bags_count, :integer, :default => 1
  end
end
