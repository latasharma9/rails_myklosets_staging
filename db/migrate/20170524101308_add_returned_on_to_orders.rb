class AddReturnedOnToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :returned_on, :date
  end
end
