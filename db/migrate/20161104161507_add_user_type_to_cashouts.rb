class AddUserTypeToCashouts < ActiveRecord::Migration
  def change
    add_column :cashouts, :user_type, :string
  end
end
