class CreateUserSubscriptions < ActiveRecord::Migration
  def change
    create_table :user_subscriptions do |t|
      t.float :amount
      t.string :payment_type
      t.boolean :payment_status
      t.string :card_number
      t.boolean :status, default: true
      t.string :transaction_id
      t.references :user
      t.references :subscription
      t.timestamps null: false
    end
  end
end
