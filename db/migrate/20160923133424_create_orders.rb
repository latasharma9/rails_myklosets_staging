class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :status
      t.float :amount
      t.string :payment_status
      t.string :payment_type
      t.string :transcation_id      
      t.integer :item_quality_rating
      t.integer :shipping_speed_rating
      t.references :user
      t.references :product

      t.timestamps null: false
    end    
  end
end
