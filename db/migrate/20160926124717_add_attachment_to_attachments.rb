class AddAttachmentToAttachments < ActiveRecord::Migration
  def change
    change_table :attachments do |t|
        t.integer :attachable_id
        t.string :attachable_type
        t.has_attached_file :attachment
    end
  end
end
