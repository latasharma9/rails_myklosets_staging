== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version (2.3.1)
    Install ruby through rvm using the following commend,
        "rvm install ruby-2.3.1"
        rvm use --default ruby-2.3.1@rails4.2.7.1 --create

* System dependencies
    Install the project gems and its dependencies by using the following command,
        "bundle install"

* Configuration
    * Configure the ActionMailer to send the mail
    * Create config/database.yml and give the database connection parameters such as
        adapter, database, username, password

* Database creation
    if development environment, plesae use the below command
        "rake db:create"
    if production environment, pelase use the below command
        RAILS_ENV=production rake db:create

* Database initialization
    if development environment, plesae use the below command
        "rake db:migrate"
    if production environment, pelase use the below command
        "RAILS_ENV=production rake db:migrate"

* Run the seed to load the initial data into the tables
    if development environment, plesae use the below command
        "rake db:seed"
    if production environment, pelase use the below command
        "RAILS_ENV=production rake db:seed"

* Run the assets precompile in prodution environment
    "RAILS_ENV=production bundle exec rake assets:precompile --trace"

* Install Imagemagick for attachment upload.
sudo apt-get install imagemagick

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

*Push notification initial setup run
	rails console
	Notification.android_push_notification_setup()
	Notification.ios_push_notification_setup()

* Update constants.yml file for the s3 credentials of each environment

*Need to update welcome email contact us,terms and privacy policy link

*Need to update cashout link in email

*Need to truncate user regids tale for updated code in gcm register

*Need to test cron job


*Need to create subscription in seed.
* Deployment instructions
    

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
