# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :environment, "production"
set :output, "log/cron_job.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every 1.day, :at => '00:01 am' do
  rake "mycloset:update_product_status"
end
every 1.day, :at => '00:10 am' do
  rake "mycloset:update_expired_subscription_user"
end
every 1.day, :at => '06:23 pm' do
  rake "mycloset:schedule_transfers_to_sellers"
end
every 1.day, :at => '11:30 pm' do
  rake "mycloset:schedule_transfers_to_buyers"
end
every 1.day, :at => '08:30 am' do
  rake "mycloset:schedule_transfers_to_buyers"
end
