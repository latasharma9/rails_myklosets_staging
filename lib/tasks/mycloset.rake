namespace :mycloset do
  desc "Update sold out product status after 2 weeks"
  task update_product_status: :environment do
    p "update_product_status job started"
    orders = Order.where(:created_at =>  2.week.ago..2.week.ago-2.days).includes(:product)
    products_id = Order.where(:created_at =>  2.week.ago..2.week.ago-2.days).pluck(:product_id)
    Product.where(:id=>products_id).update_all(:status=>false)
    p "update_product_status job end"
  end

  task update_expired_subscription_user: :environment do
    p "update_expired_subscription_user job started"
    all_subscription = UserSubscription.where(:status => true, :created_at => 1.year.ago..1.year.ago-2.days)
    all_subscription.update_all(:status=>false)
    p "update_expired_subscription_user job end"
  end

  task schedule_transfers_to_sellers: :environment do
    p "schedule_transfers_to_sellers job started"
    Cashout.check_payable_orders    
    p "schedule_transfers_to_sellers job ends"
  end

  task schedule_transfers_to_buyers: :environment do
    p "schedule_transfers_to_buyers job started"
    Cashout.check_payable_returns
    p "schedule_transfers_to_buyers job started"
  end

  task email_reminder_notificaition: :environment do
    p "email_reminder_notificaition job started"
    Cashout.check_payable_returns
    p "email_reminder_notificaition job started"
  end

end
