ActiveAdmin.register Type, :as => "Product Types" do
  menu :priority => 4
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  actions :all, except: [:new, :destroy]
  index do
  	# selectable_column
  	column :name
  	column "Created On", :created_at, :sortable => "types.created_at" do |product_type|
  	    product_type.created_at.strftime("%d %B %Y")
  	end
  	actions
  end

  csv do
    column("Name")  { |product_type| product_type.try(:name) }
    column("Created On")  { |product_type| product_type.try(:created_at).strftime("%d %B %Y") }
  end
  
  filter :name
  filter :created_at, :label=>"Created On"

  show do
    panel "Product Type Details" do
      attributes_table_for product_types do
      	row :name
      	row("Created On") {
        	product_types.created_at.strftime("%d %B %Y")
        }
      end
  	end
  end
  
  permit_params :name
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
