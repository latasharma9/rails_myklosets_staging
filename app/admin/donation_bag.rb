ActiveAdmin.register DonationBag, :as => "Donation Request" do
  menu :priority => 7
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  scope :joined, :default => true do |donation_request|
  	donation_request.includes(:user)
  end
  actions :all, except: [:new]

  member_action :approve, :method => :put do
    donation_bag = DonationBag.find params[:id]
    donation_bag.update_attributes(:status => true)
    DonationMailer.send_donation_bag_acknowdgement_notification(donation_bag).deliver
    redirect_to admin_donation_requests_path
  end

  index do
  	selectable_column
  	column "User", :user_id, :sortable => "users.uid" do |donation_request|
      link_to (donation_request.user.name || donation_request.user.uid), admin_user_path(donation_request.try(:user))
    end
  	column :shipping_address
    column :bags_count
  	column "Email Receipt", :is_receive_email_receipt, :sortable => 'donation_bags.is_receive_email_receipt' do |donation_request|
      status_tag (donation_request.is_receive_email_receipt  ? "Yes" : "No"), ((donation_request.is_receive_email_receipt ) ? :ok : :error)
    end
  	column "Created On", :created_at, :sortable => "donation_bags.created_at" do |donation_request|
  	    donation_request.created_at.strftime("%d %B %Y")
  	end
    actions defaults: false do |donation_request|
        if (donation_request.status)                    
          "Approved"
        else
          link_to 'Approve', approve_admin_donation_request_path(donation_request.id), method: :put, :data => {:confirm => "Are you sure you want to approve this request?"}
        end
    end
  	actions
  end

  csv do
    column("User")  { |donation_request| donation_request.try(:user).try(:uid) }
    column("Shipping Address")  { |donation_request| donation_request.try(:shipping_address)}
    column("Email Receipt")  { |donation_request| donation_request.try(:is_receive_email_receipt)  ? "Yes" : "No" }
    column("Created On")  { |donation_request| donation_request.try(:created_at).strftime("%d %B %Y") }
    column("Is Approved?")  { |donation_request| donation_request.try(:status) ? "Approved" : "Not Approved"}
  end
  
  filter :user, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('users') ? User.where("id > ?",0).pluck(:uid, :id) : []
  filter :is_receive_email_receipt, as: :Select, collection: [["YES",1],["NO",0]], :label=>"Email Receipt"
  filter :created_at, :label=>"Created On"

  show do
    panel "Donation Request Details" do
      attributes_table_for donation_request do
      	row("User") {
        	link_to donation_request.try(:user).try(:uid), admin_user_path(donation_request.try(:user))
        }
        row :shipping_address
        row("Email Receipt") {
        	donation_request.is_receive_email_receipt ? "Yes" : "No"
        }
        row("Posted On") {
        	donation_request.created_at.strftime("%d %B %Y")
        }
      end
  	end
  end

  form do |f|
  f.inputs "Donation Request Form" do
    f.input :user_id, as: :select, :collection => User.where("id > ?",0).pluck(:uid, :id),:include_blank => "Select a user"
    f.input :shipping_address
    f.input :is_receive_email_receipt   
  end
  f.actions
  end

permit_params :user_id, :shipping_address, :is_receive_email_receipt
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
