ActiveAdmin.register AdminUser do
  # menu :parent => "Users", :url => "#{AppConfig.url}/admin_users"
  menu :priority => 2
  permit_params :email, :password, :password_confirmation, :is_sub_admin

  index do
    selectable_column
    # id_column
    column :email
    column :is_sub_admin, :sortable => 'admin_users.is_sub_admin' do |admin_user|
        status_tag (admin_user.is_sub_admin ? "YES" : "NO"), ((admin_user.is_sub_admin) ? :ok : :error)
    end
    column :current_sign_in_at
    column :sign_in_count
    actions
  end

  filter :email
  filter :sign_in_count

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :is_sub_admin, :input_html => {:checked=>"checked"}
    end
    f.actions
  end

  show do
    panel "Admin User Details" do
      attributes_table_for admin_user do
        attributes_table_for admin_user, :email, :sign_in_count, :current_sign_in_at, :current_sign_in_ip, :last_sign_in_at, :last_sign_in_ip
      end
    end
  end

end
