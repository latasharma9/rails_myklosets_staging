ActiveAdmin.register User do

  menu :priority => 3 #, :url => "#{AppConfig.url}/users" #, :if => proc{ User.toggle_main_menu(current_admin_user) }
  actions :all, except: [:new,:edit]

  index do
    selectable_column
    column :name
    column :email
    column :user_type    
    column :nick_name
    column :created_at
    column :current_sign_in_at
    column :sign_in_count
    actions
  end

  csv do
    column("Name")  { |user| user.try(:name) }
    column("email")  { |user| user.try(:email) }
    column("user_type")  { |user| user.try(:user_type) }
    column("nick_name")  { |user| user.try(:nick_name) }
    column("created_at")  { |user| user.try(:created_at) }
    column("current_sign_in_at")  { |user| user.try(:current_sign_in_at) }
    column("sign_in_count")  { |user| user.try(:sign_in_count) }    
  end

  filter :name
  filter :email
  filter :user_type
  filter :sign_in_count

  show do
    panel "User Details" do
      attributes_table_for user do
      	attributes_table_for user, :name, :email, :user_type, :nick_name, :sign_in_count, :current_sign_in_at, :current_sign_in_ip, :last_sign_in_at, :last_sign_in_ip
      end
    end
  end

end
