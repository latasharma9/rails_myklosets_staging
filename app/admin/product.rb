ActiveAdmin.register Product do
  menu :priority => 5
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
# 
  scope :joined, :default => true do |product|
  	product.includes(:user, :type, :attachments, :product_user_visits)
  end
  actions :all, except: [:new, :destroy]

  member_action :donate, :method => :put do
    product = Product.find params[:id]
    product.update_attributes(:is_donated => true)
    redirect_to admin_products_path
  end

  index do
    column "Image", :input_html=> {:style => "width:30px;"} do |product|
      image_tag product.attachments.present? ? "http:"+product.attachments.first.attachment.url(:small) : "No Image", :style => "max-width:30px"
    end
    column :headline
    column "Product<br/>type".html_safe, :type, :sortable => "types.name"
    column "Original<br/>Price".html_safe, :original_price
    column "Selling<br/>Price".html_safe, :selling_price
    column :status, :sortable => 'products.status' do |product|
        status_tag (product.status ? "Active" : "Inactive"), ((product.status) ? :ok : :error)
    end
    column :is_sold
    column :is_deleted
    column "Posted By", :user_id, :sortable => "users.uid" do |product|
        link_to (product.user.name || product.user.uid), admin_user_path(product.try(:user))
    end
    column "Posted On", :created_at, :sortable => "products.created_at" do |product|
        product.created_at.strftime("%d %B %Y")
    end
    column "User Views" do |product|
      product.product_user_visits.length
    end
    actions defaults: false do |product|
        if (product.is_donated)                    
          "Donated"
        else
          link_to 'Mark As Donated', donate_admin_product_path(product.id), method: :put, :data => {:confirm => "Are you sure?"}
        end
    end
    actions
  end

  csv do
    column("Product type")  { |product| product.try(:type).try(:name) }
    column("Headline")  { |product| product.try(:headline) }
    column("Original Price")  { |product| product.try(:original_price) }
    column("Selling Price")  { |product| product.try(:selling_price) }
    column("Status")  { |product| product.try(:status) ? "Active" : "Inactive" }
    column("Is Sold")  { |product| product.try(:is_sold) }
    column("Is Donated")  { |product| product.try(:is_donated) }
    column("Posted By")  { |product| product.try(:user).try(:uid) }
    column("Posted On")  { |product| product.try(:created_at).strftime("%d %B %Y") }
  end

  filter :headline
  filter :description
  filter :selling_price
  filter :size, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('sizes') ? Size.where("id > ?",0).pluck(:bust, :id) : []
  filter :cup
  filter :condition
  filter :fabric
  filter :type
  filter :is_donated
  filter :created_at, :label => "Posted On"
  filter :user, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('users') ? User.where("id > ?",0).pluck(:uid, :id) : [], :label => "Posted By"

  show do
    panel "Product Details" do
      attributes_table_for product do
      	row :headline
      	row :description
      	row :selling_price
      	row :cup
      	row("Size") {
        	product.try(:size).try(:bust)
        }
      	row :condition
      	row :fabric
      	row :type      	
        row("Status") {
        	product.status ? "Active" : "Inactive"
        }
        row("Is Sold") {
        	product.is_sold ? "Yes" : "No"
        }
        row("Is Donated") {
          product.is_donated ? "Yes" : "No"
        }
        row("Posted By") {
        	link_to product.try(:user).try(:uid), admin_user_path(product.try(:user))
        }
        row("Posted On") {
        	product.created_at.strftime("%d %B %Y")
        }
      end
    end

    panel "Product Comments" do
      div do
        render "comments_box"
      end
  	end
  end

  sidebar "Rating", :only => :show do
	attributes_table_for product do
	  row("Product Rating") do |product|
	    p_rating = product.try(:order).try(:item_quality_rating)
	    p_rating.present? ? "#{p_rating} / 5" : "No Rating"
	  end
	  row("Shipping Rating") do |product|	    
	  	ship_rating = product.try(:order).try(:shipping_speed_rating)
	    ship_rating.present? ? "#{ship_rating} / 5" : "No Rating"
	  end
	end
  end

  form do |f|
  f.inputs "Product Details" do
    f.input :headline
    f.input :description
    f.input :selling_price
    f.input :cup, :include_blank => "Select a cup"
    f.input :size_id, as: :select, :collection => Size.where("id > ?",0).pluck(:bust, :id),:include_blank => "Select a size"
    f.input :condition_id, as: :select, :collection => Condition.where("id > ?",0).pluck(:name, :id),:include_blank => "Select a condition"
    f.input :fabric_id, as: :select, :collection => Fabric.where("id > ?",0).pluck(:name, :id),:include_blank => "Select a fabric"
    f.input :type_id, as: :select, :collection => Type.where("id > ?",0).pluck(:name, :id),:include_blank => "Select a type"
    f.input :is_donated, :label=>"Mark As Donated"
  end
  f.actions
  end

permit_params :headline, :description, :selling_price, :cup, :size_id, :condition_id, :fabric_id, :type_id, :weight, :units, :length, :width, :height, :dimension_units, :is_donated
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
