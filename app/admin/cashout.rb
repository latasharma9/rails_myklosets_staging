ActiveAdmin.register Cashout do
  menu :priority => 9
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  config.batch_actions = false
  scope :joined, :default => true do |cashout|
  	cashout.includes(:user=>[:seller_bank_detail])
  end
  config.sort_order = "cashouts.created_at_desc"
  actions :all, except: [:new]

   member_action :pay_seller, :method => :put do
    cashout = Cashout.find params[:id]
    cashout.update_attributes(:status => true)
    redirect_to admin_cashouts_path
  end

  index do
    column "User", :order_no, :sortable => "users.uid" do |cashout|
      link_to cashout.try(:user).try(:uid), admin_user_path(cashout.try(:user))
    end
  	column :user_type
  	column "Orders Count", :orders_count do |cashout|
  		link_to "#{cashout.try(:orders).try(:count)}", admin_orders_path(:cashout_id=>cashout.try(:id))
  	end
  	column :amount
	column "Transaction Id", :transcation_id, :sortable => 'cashouts.transcation_id' do |cashout|
		best_in_place [:admin, cashout], :transcation_id, :type => :input, :path => [:admin, cashout]
	end
  	column :bank_name, :sortable => "seller_bank_detail.bank_name" do |cashout|
  		cashout.try(:user).try(:seller_bank_detail).try(:bank_name)
  	end
  	column "Account<br/>Number".html_safe, :account_number, :sortable => "seller_bank_detail.account_number" do |cashout|
  		cashout.try(:user).try(:seller_bank_detail).try(:account_number)
  	end
  	column "Routing<br/>Number".html_safe, :routing_number, :sortable => "seller_bank_detail.routing_number" do |cashout|
  		cashout.try(:user).try(:seller_bank_detail).try(:routing_number)
  	end
  	column "Requested<br/>On".html_safe, :created_at, :sortable => "cashouts.created_at" do |cashout|
  	    cashout.try(:created_at).strftime("%d %B %Y")
  	end
  	actions defaults: false do |cashout|
        if (cashout.status)
          status_tag "Paid", ((cashout.status) ? :ok : :error)
        else
          link_to "Pay the #{cashout.try(:user_type)=='Seller' ? 'Seller' : 'Buyer'}", pay_seller_admin_cashout_path(cashout.id), method: :put #, :onclick=>"return validate_transaction($(this).closest('tr').children('.col-transaction_id').find('span').html());"
        end
    end
  end

  csv do
    column("User")  { |cashout| cashout.try(:user).try(:uid) }
    column("User Type")  { |cashout| cashout.try(:user_type) }
    column("Orders Count")  { |cashout| cashout.try(:orders).try(:count) }
    column("Amount")  { |cashout| cashout.try(:amount)}
    column("Transaction Id")  { |cashout| cashout.try(:transcation_id) }
    column("Bank Name")  { |cashout| cashout.try(:user).try(:seller_bank_detail).try(:bank_name) }
    column("Account Number")  { |cashout| cashout.try(:user).try(:seller_bank_detail).try(:account_number) }
    column("Routing Number")  { |cashout| cashout.try(:user).try(:seller_bank_detail).try(:routing_number) }
    column("Requested On")  { |cashout| cashout.created_at.strftime("%d %B %Y") }
    column("Status")  { |cashout| cashout.try(:status) ? "Paid" : "Not Paid" }
  end
  
  filter :user_id, as: :Select, collection: ActiveRecord::Base.connection.table_exists?('users') ? User.where(:id=>Cashout.pluck(:user_id).try(:uniq)).pluck(:uid, :id) : [], :label=>"Seller Name"
  filter :amount
  filter :status, as: :Select, collection: [["Paid",1],["Not Paid",0]], :label=>"Status"

  controller do
    def update
        cashout = Cashout.find_by_id(params[:id])
        respond_to do |format|
            transaction_id = params[:cashout][:transcation_id].to_i
            if (transaction_id != "" && transaction_id != "-" && transaction_id > 0)
              cashout.update_attributes(:transcation_id=> params[:cashout][:transcation_id])
              format.html { redirect_to(cashout, :notice => 'Transaction Id was successfully updated.') }
              format.json { respond_with_bip(cashout) }
            else
              format.html { render :action => "edit"}
              if (transaction_id == "" || transaction_id == "-" || transaction_id <= 0)
                format.json { render :json => {:status => "empty_trans", :value => cashout.try(:transcation_id) } }
              else
                format.json { render :json => {:status => false, :value => cashout.try(:transcation_id) } }
              end
            end
        end
    end
  end
    
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :transcation_id

#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
