ActiveAdmin.register Subscription do

menu :priority => 8
  actions :all, except: [:new, :destroy]
  member_action :make_active, :method => :put do
    subscription = Subscription.find params[:id]
    subscription.update_attributes(:status => subscription.status ? false : true)
    redirect_to admin_subscriptions_path
  end

  index do
  	selectable_column
  	column :name
  	column "Amount($)", :amount
  	column :status
  	column :months
  	column "Created On", :created_at, :sortable => "subscriptions.created_at" do |subscription|
  	    subscription.created_at.strftime("%d %B %Y")
  	end
    actions defaults: false do |subscription|
        if (subscription.status)                    
          link_to 'Inactivate', make_active_admin_subscription_path(subscription.id), method: :put
        else
          link_to 'Activate', make_active_admin_subscription_path(subscription.id), method: :put
        end
    end
  	actions
  end

  csv do
    column("Name")  { |subscription| subscription.try(:name) }
    column("Amount($)")  { |subscription| subscription.try(:amount)}
    column("Status")  { |subscription| subscription.try(:status)  ? "Active" : "Inactive" }
    column("Months")  { |subscription| subscription.try(:months)}
    column("Created On")  { |subscription| subscription.try(:created_at).strftime("%d %B %Y") }    
  end
  
  filter :name
  filter :amount
  filter :status, as: :Select, collection: [["Active",1],["Inactive",0]], :label=>"Status"
  filter :months
  filter :created_at, :label=>"Created On"

  show do
    panel "Subscription Details" do
      attributes_table_for subscription do
      	row :name
        row :amount
        row("Status") {
        	subscription.status ? "Active" : "Inactive"
        }
        row :months
        row("Created On") {
        	subscription.created_at.strftime("%d %B %Y")
        }
      end
  	end
  end

  # form do |f|
  # f.inputs "Donation Request Form" do
  #   f.input :user_id, as: :select, :collection => User.where("id > ?",0).pluck(:uid, :id),:include_blank => "Select a user"
  #   f.input :shipping_address
  #   f.input :is_receive_email_receipt    
  # end
  # f.actions
  # end
#
permit_params :name, :amount, :status, :months
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
