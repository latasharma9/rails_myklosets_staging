ActiveAdmin.register Offer do
  menu :priority => 10
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  config.sort_order = "offers.price_desc"
  
   member_action :toggle_active, :method => :put do
    offer = Offer.find params[:id]
    offer.update_attributes(:status => offer.try(:status) ? false : true)
    redirect_to admin_offers_path
  end

  index do
  	selectable_column
  	column :name
  	column :price	
	column "Status", :status, :sortable => "offers.status" do |offer|
  	    status_tag (offer.status ? "Active" : "Inactive"), ((offer.status) ? :ok : :error)
  	end
  	column "Created On", :created_at, :sortable => "offers.created_at" do |offer|
  	    offer.try(:created_at).strftime("%d %B %Y")
  	end
  	actions defaults: false do |offer|
        if (offer.status)
          link_to 'Inactivate', toggle_active_admin_offer_path(offer.id), method: :put
          
        else
          link_to 'Activate', toggle_active_admin_offer_path(offer.id), method: :put #, :onclick=>"return validate_transaction($(this).closest('tr').children('.col-transaction_id').find('span').html());"
        end
    end
    actions
  end

  csv do
    column("Name")  { |offer| offer.try(:name) }
    column("Price")  { |offer| offer.try(:price)}
    column("Status")  { |offer| offer.try(:status) ? "Active" : "Inactive" }
    column("Created On")  { |offer| offer.created_at.strftime("%d %B %Y") }    
  end
  
  filter :name
  filter :price
  filter :status, as: :Select, collection: [["Active",1],["Inactive",0]], :label=>"Status"

  show do
    panel "Offer Details" do
      attributes_table_for offer do
      	row :name
        row :price
        row("Status") {
        	offer.status ? "Active" : "Inactive"
        }
        row("Created On") {
        	offer.created_at.strftime("%d %B %Y")
        }
      end
  	end
  end

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :name, :price, :status
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
