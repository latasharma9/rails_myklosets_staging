ActiveAdmin.register Color do

menu :priority => 8
  actions :all, except: [:destroy]
  # member_action :make_active, :method => :put do
  #   subscription = Color.find params[:id]
  #   subscription.update_attributes(:status => subscription.status ? false : true)
  #   redirect_to admin_subscriptions_path
  # end

  index do
  	column :name
  	column :code
  	column "Color" do |color|
    	span ".....", :style=>"height:35px;width:35px;background:#{color.code}; color:#{color.code}", :class => "color-circle"
  	end
  	column "Created On", :created_at, :sortable => "colors.created_at" do |color|
  	    color.created_at.strftime("%d %B %Y")
  	end    
  	actions
  end

  csv do
    column("Name")  { |color| color.try(:name) }
    column("Code")  { |color| color.try(:months)}
    column("Created On")  { |color| color.try(:created_at).strftime("%d %B %Y") }    
  end
  
  filter :name
  filter :code
  filter :created_at, :label=>"Created On"

  show do
    panel "Color Details" do
      attributes_table_for color do
      	row :name
        row :code
        row("Created On") {
        	color.created_at.strftime("%d %B %Y")
        }
      end
  	end
  end

  # form do |f|
  # f.inputs "Donation Request Form" do
  #   f.input :user_id, as: :select, :collection => User.where("id > ?",0).pluck(:uid, :id),:include_blank => "Select a user"
  #   f.input :shipping_address
  #   f.input :is_receive_email_receipt    
  # end
  # f.actions
  # end
#
permit_params :name, :code
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
