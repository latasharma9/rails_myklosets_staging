class DonationMailer < ActionMailer::Base
    default from: "myKlosets <admin@myklosets.com>"
    def send_donation_bag_request_notification(donation_bag)
        @donator_email = donation_bag.user.email
        @donator_name = donation_bag.user.name || donation_bag.user.uid
        mail(to: donation_bag.user.email, subject: "Donation bag requested.")
    end

    # send email to buyer when order has been created
    def send_donation_bag_acknowdgement_notification(donation_bag)
        @donator_email = donation_bag.user.email
        @donator_name = donation_bag.user.name || donation_bag.user.uid
        mail(to: donation_bag.user.email, subject: "Your donation bag is on the way!!")
    end

    # send email to buyer when order has been created
    def send_donation_bag_request_admin_notification(donation_bag)
        @donator_email = donation_bag.user.email
        @donator_name = donation_bag.user.name || donation_bag.user.uid
        mail(to: "admin@myklosets.com", subject: "We have got a request for donation bag.")
    end
end