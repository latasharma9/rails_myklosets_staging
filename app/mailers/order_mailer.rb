class OrderMailer < ActionMailer::Base
    default from: "myKlosets <admin@myklosets.com>"
    def send_email_order_notification_to_seller(product, message)
        @message = message
        email_order_details(product.order)
        mail(to: @product.user.email, subject: "You have received new Order(#{@order_number})")
    end

    # send email to buyer when order has been created
    def send_email_order_notification_to_buyer product
        @user_name = product.order.user.name || product.order.user.uid
        email_order_details(product.order)
        mail(
            to: @buyer_email,
            subject: "Order#(#{@order_number}) Confirmation"
        )
    end

    # Send mail to seller when buyer confirm and accepts the delivery
    def email_to_seller_buyer_confirm_accept_delivery(order)
        @user_name = order.product.user.name || order.product.user.uid
        email_order_details(order)
        mail(
            to: @seller_email,
            subject: "Buyer confirms and accepts the Order#(#{@order_number}) delivery"
        )
    end

    # send mail to seller when return request made from buyer
    def send_email_to_seller_product_return(product,reason)        
        @user_name = product.user.name || product.user.email || product.user.uid
        @return_reason = reason
        email_order_details(product.order)
        mail(
            to: @seller_email,
            subject: "Buyer made Order#(#{@order_number}) return request"
        )
    end

    # Send mail to buyer when seeler acknowledge the product return back
    def email_to_buyer_seller_ack_product_back(order)
        @user_name = order.user.name || order.user.email || order.user.uid
        email_order_details(order)
        mail(
            to: @buyer_email,
            subject: "Seller acknowledges Order#(#{@order_number}) return back"
        )
    end

    # Send mail to admin when seller acknowledge the product return back
    def email_to_admin_seller_ack_product_back(order)
        @admin_email = "admin@myklosets.com" #AdminUser.first.email
        @order_id = order.id
        @price_amount = order.sellers_earnings + order.my_closet_earnings        
        email_order_details(order)
        mail(
            to: @admin_email,
            subject: "#{@seller_name} has accepted the return of a product with order#{@order_number} from #{@buyer_name}"
        )
    end

    # Send mail to buyer when seller made pre shipment the order
    def email_to_buyer_seller_pre_shipment(order)
        email_order_details(order)
        mail(
            to: @buyer_email,
            subject: "Seller pre shipment Order#(#{@order_number})"
        )
    end

    # Send mail to buyer did not receive the item
    def email_to_buyer_order_not_received(order)
        email_order_details(order)
        mail(
            to: @buyer_email,
            subject: "Order(#{@order_number}) not received"
        )
    end

    # Send mail to seller did not receive the item
    def email_to_seller_order_not_received(order)
        email_order_details(order)
        mail(
            to: @seller_email,
            subject: "Order(#{@order_number}) not received"
        )
    end

    # Send mail to seller did not receive the item
    def email_to_buyer_reminding_order_acceptance(order)
        email_order_details(order)
        mail(
            to: @buyer_email,
            subject: "Order(#{@order_number}) acceptance reminder"
        )
    end

    def email_order_details(order)
        @buyer_email = order.user.email
        @buyer_name = order.user.name || order.user.uid
        @seller_email = order.product.user.email
        @seller_name = order.product.user.name || order.product.user.uid
        @product = order.product
        @product_img = 'http:'+@product.attachments.first.attachment.url(:small)
        @product_headline = @product.headline
        @order_number = order.order_no
        @order_date = @product.order.created_at.strftime("%A %B, %d")
        @selling_price = '%.2f' % [(@product.selling_price * 100).round / 100.0] 
        @order_amount = '%.2f' % [(@product.order.amount * 100).round / 100.0] 
        @payment_charges = '%.2f' % [((@product.order.amount - @product.selling_price) * 100).round / 100.0] 

        if order.shipping_address_id.nil?
            shipping_address = order.user.shipping_addresses.try(:first)
        else
            shipping_address = order.shipping_address
        end
        @addr1 = shipping_address.street_address
        @addr2 = [shipping_address.city, shipping_address.state, shipping_address.zip_code.to_s].join(',')
    end

end
