class Product < ActiveRecord::Base
	belongs_to :user
	belongs_to :type
	belongs_to :fabric
	belongs_to :condition
	belongs_to :size
	belongs_to :shirt_size
	belongs_to :cup
	has_one :order #, :dependent => :destroy
	has_many :item_comments, :dependent => :destroy
	has_many :wishlists, :dependent => :destroy
	has_many :notifications, :dependent => :destroy
	has_many :attachments, :as => :attachable, :dependent => :destroy
	has_many :colors, :through => :color_products
	has_many :color_products, :dependent => :destroy
	has_many :product_user_visits, :dependent => :destroy
	has_one :product_image, :dependent => :destroy

	scope :by_type, -> type { where(type_id: type)}

	scope :by_color, -> color { where(color_id: color)}

	scope :by_fabric, -> fabric { where(fabric_id: fabric)}

	scope :by_condition, -> condition { where(condition_id: condition)}

	scope :sort_by_latest, -> type { where(type_id: type)}

	validates :headline, presence: true
	validates :description, presence: true


    def self.by_latest_order
        order('created_at DESC')
    end

    def get_user_subscription
        self.user.is_user_subscribed
    end

	def self.get_sorted_products sort_by
		if sort_by == 'new'
            includes(:wishlists, :attachments).
			order('created_at DESC')
		elsif sort_by == 'price_low_to_high'
            includes(:wishlists, :attachments).
			order('selling_price ASC')
		elsif sort_by == 'price_high_to_low'
            includes(:wishlists, :attachments).
			order('selling_price DESC')
		elsif sort_by == 'popular'
            includes(:attachments).
			# select('products.*, COUNT(wishlists.id) AS wishlists_count').
            includes(:wishlists).
            order('wishlists_count DESC')		
		else
			return 'Invalid'
		end
	end

	def self.prepare_objects(products,user)
		user_product_loves = user.wishlists.pluck(:product_id)            
        data_array = []
        if products.present?
            products.each do |product|
                data_hash = {}
                data_hash["id"] = product.id
                data_hash["headline"] = product.headline
                data_hash["selling_price"] = product.selling_price
                data_hash["original_price"] = product.original_price
                data_hash["total"] = product.total
                data_hash["is_sold"] = product.is_sold
                data_hash["is_rent"] = product.is_rent
                data_hash["is_donated"] = product.is_donated
                primary_product_image(product, data_hash)
                data_hash["wishlists"] = product.wishlists.length
                data_hash["is_loved"] = user_product_loves.include? product.id
                data_hash["user_name"] = user.name
                data_array.append(data_hash)
            end
        end
        return data_array
	end

	def self.prepare_sales_objects(products,user)
		user_product_loves = user.wishlists.pluck(:product_id)            
        data_array = []
        if products.present?
                products.each do |product|
                    data_hash = {}
                    #data_hash["info"] = product
                    data_hash["id"] = product.order.id
                    data_hash["headline"] = product.headline
                    data_hash["selling_price"] = product.selling_price
                    data_hash["total"] = product.total
                    data_hash["status"] = product.order.status
                    data_hash["amount"] = product.order.amount
                    data_hash["is_cashout_requested"] = product.order.amount
                    data_hash["is_rent"] = product.is_rent
                    data_hash["is_donated"] = product.is_donated
                    data_hash["sellers_earnings"] = product.order.sellers_earnings
                    data_hash["attachment"] = \
                        product.attachments.present? ? 'http:'+product.attachments.first.attachment.url(:small) : ''                     
					# data_hash["wishlists"] = product.wishlists.length
               		data_hash["is_loved"] = user_product_loves.include? product.id                        
                    data_array.append(data_hash)
                end
            end
        return data_array
	end

	def self.primary_product_image(product,data_hash)
		primary_image = product.product_image		
	    primary_image ? primary_product = primary_image.attachment : primary_product = nil
	    if primary_product
	        data_hash["attachments"] = \
	            product.attachments.present? ? {:url => 'http:'+primary_product.attachment.url(:small),
	                :caption => primary_product.attachment.try(:caption).try(:title) || ''} : ''
	    else
	    	data_hash["attachments"] = \
	            product.attachments.present? ? {:url => 'http:'+product.attachments.first.attachment.url(:small),
	                :caption => product.attachments.first.attachment.try(:caption).try(:title) || ''} : ''
	    end
	end

	def self.prepare_cashout_objects(products)          
        data_array = []
        if products.present?
                products.each do |product|                  
                    data_array.append(product.order.id)
                end
            end
        return data_array
	end

	def self.search_by_text(search_query)
		
		colors = Color.all
		fabrics = Fabric.all
		conditions = Condition.all
		sql = "select * from types"
		types = ActiveRecord::Base.connection.execute(sql)
		# types = Type.all
		type_id, color_id, fabric_id, condition_id = nil
		default_search_string = ""
		default_search_values = []
		@base_products = Product.where("headline like ? OR description like ?","%#{search_query}%","%#{search_query}%").pluck(:id)
		# @products = Product.limit(20)
		@query_products = []
		search_query.split(" ").each_with_index do |string,counter|
			# conditions.each do |condition|
			# 	if string == condition.name
			# 		p "condition"
			# 		p condition.name
			# 		condition_id = condition.id
			# 	end
			# end
			# colors.each do |color|
			# 	if string == color.name
			# 		color_id = color.id
			# 	end
			# end
			# fabrics.each do |fabric|
			# 	if string == fabric.name
			# 		p "fabric"
			# 		p fabric.name
			# 		fabric_id = fabric.id
			# 	end
			# end
			# types.each do |type|
			# 	if string == type.name
			# 		type_id = type.id
			# 	end
			# end
			# default_search_string << " OR " if default_search_string != ""
			# default_search_string << "headline like ? OR description like ?"
			# default_search_values.push("%#{string}%")
			# default_search_values.push("%#{string}%")			
			@query_products = Product.where("headline like ? OR description like ?","%#{string}%","%#{string}%").pluck(:id) if counter==0
			@query_products << Product.where("headline like ? OR description like ?","%#{string}%","%#{string}%").pluck(:id) if counter>0
		end
		# @products = @products.where(default_search_string,default_search_values.join(","))
		# @query_products = @query_products.by_condition(condition_id) if condition_id!=nil
		# @products = @products.by_type(type_id) if type_id!=0
		# @query_products = @query_products.by_fabric(fabric_id) if fabric_id!=nil
		# @products = @products.by_color(color_id) if color_id!=0
		result  = @base_products << @query_products
		return Product.where(:id => result.flatten.uniq)
		
		
	end
end
