class Attachment < ActiveRecord::Base
    has_one :caption
	belongs_to :attachable, :polymorphic => true
	has_one :product_image, :dependent => :destroy
    has_attached_file :attachment, :styles => {:original => "600x600", :medium => "350x350", :small => "180x180" }
                    #:url  => "/assets/attachments/:id/:style/:basename.:extension",
                    #:path => ":rails_root/public/assets/attachments/:id/:style/:basename.:extension"
    validates_attachment :attachment, content_type: { content_type: ["image/png", "image/jpg", "image/jpeg"] }
end
