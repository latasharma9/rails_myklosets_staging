class DonationBag < ActiveRecord::Base
	belongs_to :user

	after_create :send_mailer

	def send_mailer
		Thread.new { 
              DonationMailer.send_donation_bag_request_admin_notification(self).deliver
              DonationMailer.send_donation_bag_request_notification(self).deliver
        }	
	end
end
