class Cashout < ActiveRecord::Base
	attr_accessor :orders_count
	has_many :orders
	belongs_to :user

	require "stripe"
	# Stripe.api_key = "sk_live_ATcVsDGnKt25oTcJipTvLsxg"
	
	Stripe.api_key =  "sk_test_fmmcKGEy2bZrXHgebSIG6OEb"

	
	def self.test_charge
		card_token = Stripe::Token.create(
		  :card => {
		    :number => "4242424242424242",
		    :exp_month => 11,
		    :exp_year => 2019,
		    :cvc => "786"
		  },
		)
		Stripe::Charge.create(
		  :amount => 250,
		  :currency => "usd",
		  :source => card_token[:id], # obtained with Stripe.js
		  :description => "Testing transaction for myKloset app"
		)
	end

	def self.check_and_process_user_account_payment(userid, order)
		user_obj = User.find_by(id: userid)
		if user_obj.stripe_account_id.nil?
			unless user_obj.seller_bank_detail.nil?
				user_account = Stripe::Account.create(
				  {
				    :country => 'US',
				    :managed => true,
				    :legal_entity => {
				    	:first_name => user_obj.first_name || user_obj.name,
				      	:last_name =>  user_obj.last_name || user_obj.name,
				    	:type => "individual",
				    	:dob => {
					      :day=> user_obj.dob_day,
					      :month=> user_obj.dob_month,
					      :year=> user_obj.dob_year
					    }
				    },
				    :external_account => {
				      :object => 'bank_account',
				      :country => 'US',
				      :currency => 'usd',				      
				      :default_for_currency => true,
			    	  :account_holder_name => user_obj.name || user_obj.email.split('@').first,
			    	  :routing_number => user_obj.seller_bank_detail.routing_number,
			    	  :account_number => user_obj.seller_bank_detail.account_number
				    },
				    :tos_acceptance => {
				      :date => user_obj.seller_bank_detail.updated_at.to_i,
				      :ip => '52.33.60.235',
				    }
				  }
				)
				user_obj.update_attributes(:stripe_account_id=>user_account.id)
			else
				return "Cannot process payment as there are no bank details for this user"
			end
		else
			user_account =Stripe::Account.retrieve(user_obj.stripe_account_id)			
		end
		unless user_obj.seller_bank_detail.nil?			
			max_attempts = 2
			attempts = 0
			begin
				make_transfer(user_account.id,order)
			rescue Exception => ex
				p "Error: #{ex}"
				attempts = attempts + 1
	  			retry if(attempts < max_attempts)
			end
		else
			return "Cannot process payment as there are no bank details for this user"
		end
	end

	def self.check_payable_orders
		payable_orders = Order.where("shipped_on <= ? and returned_on IS NULL and transfer_id IS NULL",Date.today-7.days).includes(:product=>:user)
		payable_orders.each do |order|
			check_and_process_user_account_payment(order.product.user_id,order)#Pass seller user id
		end
	end

	def self.check_payable_returns
		payable_orders = Order.where("returned_on <= ? and transfer_id IS NULL",Date.today-7.days).includes(:product=>:user)
		payable_orders.each do |order|
			check_and_process_user_account_payment(order.user_id,order)#Pass buyer user id
		end
	end

	def self.bank_token(user_obj)
		bank_token = Stripe::Token.create(
		  :bank_account => {
		    :country => "US",
		    :currency => "usd",
		    :default_for_currency => true,
		    :account_holder_name => user_obj.name || user_obj.email,
		    :account_holder_type => "individual",
		    :routing_number => user_obj.seller_bank_detail.routing_number,
		    :account_number => user_obj.seller_bank_detail.account_number
		  }
		)
		return bank_token[:id]
	end

	def self.make_transfer(dest_acct_id,order)
		transfer_obj = Stripe::Transfer.create(
		  :amount => (order.sellers_earnings * 100).to_i,
		  :currency => "usd",
		  :destination => dest_acct_id #"acct_1ADWaMHAkzPPolz5"
		)
		if transfer_obj.status=="paid"
			order.update_attributes(:transfer_id=>transfer_obj.id,:transfer_date=>Date.today)
		end
	end

	def self.update_email
		account = Stripe::Account.retrieve("acct_1AKm0yG4ENhQy1A8")
		account.email = "tag@mc.in"
		account.save		
	end

end
