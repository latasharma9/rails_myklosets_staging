class Order < ActiveRecord::Base
	belongs_to :product
	belongs_to :user
	belongs_to :cashout
    belongs_to :shipping_address
    belongs_to :billing_address
    has_one :product_return

    attr_accessor :seller_name

    def duplicate_product
    	new_product = self.product.dup
        new_product.is_sold = false
        new_product.status = true
    	new_product.save!
    	self.product.color_products.each do |color_product|
    		new_product.color_products.create(:color_id=>color_product.color_id)
    	end
    	require 'open-uri'
    	self.product.attachments.each do |attachment|
    		new_file_path = 'http:' + attachment.attachment.url
    		new_file = open(URI.parse(new_file_path))
    		new_product.attachments.create(:attachment=>new_file)
    	end    	
    end

    def self.send_reminder_mails_for_unaccepted_orders
        pending_orders = Order.where("shipped_on = ? and transfer_id IS NULL and returned_on IS NULL",Date.today-5.days).includes(:product=>:user)
        pending_orders.each do |order|
            Thread.new { 
              OrderMailer.email_to_buyer_reminding_order_acceptance(order).deliver
            }            
        end
    end
end
