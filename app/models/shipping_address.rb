class ShippingAddress < ActiveRecord::Base
	belongs_to :user
	belongs_to :country

	def self.return_shipping_id(user_id,ba_id)
		ba = BillingAddress.find_by_id(ba_id)
        sa = ShippingAddress.where(:street_address=>ba.street_address,:city=>ba.city,:state=>ba.state,:user_id=>user_id,:zip_code=>ba.zip_code).try(:first)
        if sa.nil?
            sa = create(ba.attributes.except("id","created_at","updated_at"))
        end
        return sa.id
	end
end
