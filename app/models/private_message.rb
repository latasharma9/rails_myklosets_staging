class PrivateMessage < ActiveRecord::Base
	belongs_to :user
	belongs_to :product
	belongs_to :receiver, :class_name => "User", :foreign_key => "receiver_id"

	def self.prepare_message_object(message)
		data_hash = {
				:username => message.user.name || messagemessage.user.uid,
				:message => message.message,
				:user_id => message.user_id,
				:created_at => message.created_at
			}
	end

	def self.prepare_message_conversation(parent_message,sub_messages,receiver_id)		
		message_data = { :sub_messages => [], :parent_message => {
				:id => parent_message.id,
				:message => parent_message.message,
				:username => parent_message.user.name || parent_message.user.uid,
				:receiver_id => receiver_id,
				:product_id => parent_message.product_id,
				:user_id => parent_message.user_id,
				:user_image_url => parent_message.user.attachment.nil? ? "" : 'http:' + parent_message.user.attachment.attachment.url(:small),
				:created_at => parent_message.created_at
			}
		}
		sub_messages.each do |mesg|
			data_hash = {
				:username => mesg.user.name || mesg.user.uid,
				:user_image_url => mesg.user.attachment.nil? ? "" : 'http:' + mesg.user.attachment.attachment.url(:small),
				:message => mesg.message,
				:user_id => mesg.user_id,
				:created_at => mesg.created_at
			}
			message_data[:sub_messages] << data_hash
		end
		message_data
	end
end
