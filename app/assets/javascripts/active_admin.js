#= require active_admin/base
//= require best_in_place
//= require best_in_place.purr

$( document ).ready(function() {
	jQuery(".best_in_place").best_in_place();

	$(".best_in_place").on("ajax:success", function(evt, data, status, xhr) {
        if (xhr.responseText != "" && JSON.parse(xhr.responseText).status == "empty_trans")
        {
            alert("Please enter a valid Transaction Id");
            $("#"+evt.target.id).html(JSON.parse(xhr.responseText).value);
        }        
    });

    $('body.admin_product_types a[data-method=delete]').
        data('confirm', 'All products linked to this type will also be destroyed. Are you sure you want to continue?');

    // to hide the scoped tabs(joined) in admin index pages
    $(".table_tools").find(".scopes.table_tools_segmented_control").hide();
    // to Hide the unwanted search results section
    $("#sidebar").find("#search-status-_sidebar_section").hide();


    $(".index.admin_cashouts").find('tbody .table_actions a').click(function(event) {
    	validate_transaction(event,$(this).closest('tr').children('.col-transaction_id').find('span').html())
    });

    
});

function validate_transaction(event,trans_id)
{
	if (jQuery.isNumeric(trans_id))
	{
		return true;
	}
	else
	{
		alert("Please click on Transaction Id and enter the value")
		event.stopPropagation();
		event.preventDefault();
	}
}