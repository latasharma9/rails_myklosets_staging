=begin
This module will be used for manipulating user`s profile and it will get the requests 
from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Sep 29,2016
@author : TAG
=end
class Api::V2::ProfilesController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :authenticate_api_v1_user!
    
    # This Method used for fetch user`s profile data
    def show
        begin
            user = User.find(params[:id])
            data = new_hash
            if user
                data['info'] = user
                data['profile_image_url'] = ''
                unless user.attachment.nil?
                    data['profile_image_url'] = 'http:' + user.attachment.attachment.url(:small)
                end
                data['shipping_addresses'] = user.shipping_addresses
                data['billing_addresses'] = user.billing_addresses
            end
            render success_response_with_object(
                "sucess",
                200,
                data
            )
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for update the user profile details
    def update
        begin
            data = new_hash
            user = User.find(params[:id])
            if params[:profile].has_key?('name')
                user.update_attributes(:name => params[:profile][:name])
            end
            # Update Shipping address
            # if user.shipping_addresses.empty?
            #     profile_params[:shipping_address][:user_id] = current_api_v1_user.id
            #     ShippingAddress.create(profile_params[:shipping_address])
            # else
            #     user.shipping_addresses.first.update_attributes(profile_params[:shipping_address])
            # end
            # # Update billing address
            # if user.billing_addresses.empty?
            #     profile_params[:billing_address][:user_id] = current_api_v1_user.id
            #     BillingAddress.create(profile_params[:billing_address])
            # else
            #     user.billing_addresses.first.update_attributes(profile_params[:billing_address])
            # end
            unless user.attachment.nil?
                data['profile_image_url'] = 'http:' + user.attachment.attachment.url(:small)
            end
            data['info'] = user
            data['shipping_addresses'] = user.shipping_addresses
            data['billing_addresses'] = user.billing_addresses
            render success_response_with_object(
                "sucess",
                200,
                data
            )
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def get_address
        begin
            data = new_hash
            user = current_api_v1_user            
            data['info'] = user
            data['shipping_addresses'] = user.shipping_addresses
            data['billing_addresses'] = user.billing_addresses
            render success_response_with_object(
                "sucess",
                200,
                data
            )
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def add_address
        begin
            data = new_hash
            user = current_api_v1_user
            if params["profile"].has_key?('billing_address')
                profile_params[:billing_address][:user_id] = current_api_v1_user.id
                BillingAddress.create(profile_params[:billing_address])
            elsif params["profile"].has_key?('shipping_address')                
                profile_params[:shipping_address][:user_id] = current_api_v1_user.id
                ShippingAddress.create(profile_params[:shipping_address])
            end
            data['info'] = user
            data['shipping_addresses'] = user.shipping_addresses
            data['billing_addresses'] = user.billing_addresses
            render success_response_with_object(
                "sucess",
                200,
                data
            )
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for update the user profile image
    def update_profile_image
        begin
            user = User.find(params[:id])
            if params['file']
                if user.attachment.nil?
                    user.create_attachment(:attachment=>params['file'])
                else
                    user.attachment.update_attributes(:attachment=>params['file'])
                end
                render success_response_with_object(
                    "sucess",
                    200,
                    {
                        :user_id => user.id,
                        :url => 'http:' + user.attachment.attachment.url(:small)
                    }
                )
            else
                render error_response("Please select an Image", 400)
            end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for update the password
    def update_password
        begin
            user=User.find(params[:id])
            if user.valid_password?(params[:user][:current_password])
                if user.update(user_params)
                    render success_response_with_object(
                        "sucess",
                        200,
                        user
                    )
                else
                    error = user.errors.messages.has_key?(:password) ? "Password "+user.errors.messages[:password][0]: "Something went wrong!"
                    render error_response_with_object(error, 500, user.errors)
                end
            else
                render error_response("Current password is wrong", 400)
            end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # Update retrun policy
    def update_return_policy
        begin
            user=User.find(params[:id])
            if user == current_api_v1_user
                policy = user.seller_return_policy
                if policy.nil?
                    user.create_seller_return_policy(seller_return_params)
                    render success_response_with_object(
                        "Updated Sucessfully",
                        200,
                        user.seller_return_policy
                    )
                else
                    policy.update(seller_return_params)
                    render success_response_with_object(
                        "Updated Sucessfully",
                        200,
                        user.seller_return_policy
                    )
                end
            else
                render error_response("User is not found", 400)
            end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # Fetch retrun policy
    def fetch_return_policy
        begin
            user=User.find(params[:id])
            if user
                policy = user.seller_return_policy
                render success_response_with_object(
                    "sucess",
                    200,
                    {
                        :policy => policy,
                        :user_name => user.name
                    }
                )
            else
                render error_response("User is not found", 400)
            end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    protected
    def user_params
        params.require(:user).permit(:password, :password_confirmation)
    end

    def profile_params
        params.require(:profile).permit!
    end

    def seller_return_params
        params.require(:profile).permit!
    end

end
