class Api::V2::OrdersController < ApplicationController
	include DeviseTokenAuth::Concerns::SetUserByToken
	before_action :authenticate_api_v1_user!

    def initialize
        @limit = 1000
    end
	
	#This method is to create a new order and update a product to is_sold status
	def create
		 begin
			product = Product.find_by_id(params[:product_id])
			unless product.is_sold
                earning_calculation(product,product.selling_price)
                params[:order][:order_no] = (Random.rand(1000234...99999999)).to_s
                unless Order.find_by_order_no(params[:order][:order_no]).nil?
                    params[:order][:order_no] = (Random.rand(1000234...99999999)).to_s
                end
                if params[:same_as_billing].to_s == "true"
                    sa = ShippingAddress.create(BillingAddress.find_by_id(params[:order][:billing_address_id]).attributes.except("id"))
                    params[:order][:shipping_address_id] = sa.id
                end
	            order = Order.create(order_params)
	            unless order.errors.present?	            	
	                product.update_attributes(:is_sold => true)
                    sender = User.find_by_id(current_api_v1_user.id)
                    sender_name = sender.name || sender.uid
                    @order_message = "Congrats! "+ sender_name +" purchased "" #{product.headline}!" 
                    Thread.new { 
                        OrderMailer.send_email_order_notification_to_seller(product, @order_message).deliver
                        OrderMailer.send_email_order_notification_to_buyer(product).deliver
                        
                    }
                    update_notification('order', product, sender, @order_message)  if product && sender
                    send_order_notification(product,@order_message)
	                render success_response_with_object("sucess",200,order)
	            else
	                render error_response(order.errors.messages, 400)
	            end
	        else
	         	render error_response_with_object("Error", 500, "Product Already Sold.")
	        end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
	end

    def earning_calculation(product,selling_price)
        unless product.total.nil?
            if product.get_user_subscription
                listing_fee = 0.0
            else
                listing_fee = (selling_price*CHARGES["listing_fee"])/100 
            end
            params[:order][:amount] = ((params[:order][:amount].to_f/100).to_f).round(2)
            params[:order][:my_closet_earnings] = listing_fee
            params[:order][:sellers_earnings] = selling_price - listing_fee
        end        
    end

	#This method for get all orders placed by user
	def index
        params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
		begin
            product_sales = Product.includes(:order,:wishlists,:attachments).select(:id,:headline,:selling_price,:amount,:status,:is_rent, :sellers_earnings).where("orders.user_id= #{current_api_v1_user.id} and products.id = orders.product_id").order('orders.created_at DESC').limit(@limit).offset(offset_count)
            data_array = Product.prepare_sales_objects(product_sales,current_api_v1_user)
            render success_response_with_object("sucess",200,data_array)
		rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
	end

	#This method for get all sales by user
	def my_sales
        params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
		begin
			product_sales = Product.includes(:order,:attachments).select(:id,:headline,:selling_price,:amount,:status,:is_rent, :sellers_earnings).where("products.user_id= #{current_api_v1_user.id} and products.id = orders.product_id").order('orders.created_at DESC').limit(@limit).offset(offset_count)
            data_array = Product.prepare_sales_objects(product_sales.uniq,current_api_v1_user)            
            cash_out_query = "products.user_id= #{current_api_v1_user.id} and products.id = orders.product_id and cashout_id is null and orders.status = 'Order Delivered'"
            cash_out_amount = Product.joins(:order).where(cash_out_query).sum(:sellers_earnings).round(2)
            cash_out_products_orders = Product.joins(:order).where(cash_out_query)
            cash_out_orders = Product.prepare_cashout_objects(cash_out_products_orders)
            render success_response_with_object_sales("sucess",200,data_array,cash_out_amount,cash_out_orders)
		rescue Exception => e
             render error_response_with_object("Error", 500, e.message)
        end
	end
    

    # To update the order ratings
    def update
        begin
            order = Order.find(params[:id])
            order.update_attributes(order_rating_params)
            if order.status == 'Order Delivered'
                Thread.new{
                    OrderMailer.email_to_seller_buyer_confirm_accept_delivery(
                        order
                    ).deliver
                }
            elsif order.status == 'Return Accepted'
                cash_out = Cashout.create(
                    :user_id=>order.user_id,
                    :amount=>order.product.selling_price,
                    :user_type=>"Buyer")
                order.update_attributes(:cashout_id=>cash_out.id)
                order.product.update_attributes(:status=>false)
                if params["repost_product"] == true
                    Thread.new { 
                        order.duplicate_product 
                    }
                end
                Thread.new{
                    OrderMailer.email_to_buyer_seller_ack_product_back(
                        order
                    ).deliver
                    OrderMailer.email_to_admin_seller_ack_product_back(
                        order
                    ).deliver
                }
            elsif order.status == 'Pre-shipment'
                Thread.new{
                    OrderMailer.email_to_buyer_seller_pre_shipment(
                        order
                    ).deliver
                }
            end
            render success_response_with_object("sucess",200,order)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # To get the order details
    def show
        begin
            order = Order.find(params[:id])
            if order
                shipping_address = ''
                product_info = ''
                user = order.user
                product = order.product
                if order.shipping_address
                    shipping_address = order.shipping_address
                else
                    shipping_address = user.shipping_addresses.first
                end

                if user                    
                    user_name = user.name || user.uid
                end
                data_obj = {
                    :order => order,
                    :shipping_address => shipping_address,
                    :product => product,
                    :seller_id => product.user.id,
                    :user_name => user_name,
                    :seller_return_policy => product.user.seller_return_policy,
                    :product_return => order.try(:product_return),
                    :attachment => order.product.attachments.present? ? "http:"+product.attachments.first.attachment.url(:medium) : "img/img_product-3.jpg"
                }
                render success_response_with_object("sucess",200,data_obj)
            else
                render error_response("Not found",200)
            end
            
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def product_return
        begin
            order_return = ProductReturn.find_or_create_by(order_return_params)
            order_return.order.update_attributes(:status => params[:status]) if params[:status]
            product = order_return.order.product            
            sender = User.find_by_id(params[:user_id])
            sender_name = sender.name || sender.uid             
            @order_message = "#{sender_name} has requested to return the ordered product(Order ##{order_return.order.order_no}) for the following reason: #{order_return.reason}."
            update_notification('order', product, sender, @order_message)  if product and sender
            send_return_notification(product,@order_message)
            Thread.new {
                OrderMailer.send_email_to_seller_product_return(product, order_return.reason).deliver            
            }
            render success_response_with_object("sucess",200,order_return)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end


    def product_not_received
        begin
            order = Order.find_by_id(params[:id])
            product = order.product
            sender = order.product.user
            sender_name = sender.name || sender.uid 
            @order_message = "#{product.headline} with order number (#{product.order.order_no}) is not yet received by buyer, please follow the shipment status."
            update_notification('order', product, sender, @order_message)  if product and sender
            send_notification_order_not_received(product, @order_message)
            Thread.new {
                OrderMailer.email_to_buyer_order_not_received(product.order).deliver
                OrderMailer.email_to_seller_order_not_received(product.order).deliver
            }
            render success_response_with_object("sucess",200,product)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # send notification to product owner for product not received
    def send_notification_order_not_received(product,message)
        product_owner = find_by_user(product.user.id)
        title = "Order Not Received"
        message = message
        body = product.headline 
        product_owner_android_reg_id = product.user.user_regids.where("user_regids.device_os='android'").pluck(:reg_id).uniq
        product_owner_ios_reg_id = product.user.user_regids.where("user_regids.device_os='ios'").pluck(:reg_id).uniq
        android_push_notification(product_owner_android_reg_id,message,title,body) if !product_owner_android_reg_id.empty?
        ios_push_notification(product_owner_ios_reg_id,message,title,body) if !product_owner_ios_reg_id.empty?

        #Push Notification Product owner end
    end

	#To send push notification to product owner for order placed
    def send_order_notification(product,message)
        #Notification.create(private_message_params)
        #Push Notification Product owner start
        order_placed_by = find_by_user(current_api_v1_user.id)
        title = "New Order Received"
        message = message
        body = product.headline #+"Order by "+ order_placed_by.name || order_placed_by.email
        # product_owner_reg_id = product.user.user_regids.pluck(:reg_id).uniq!
        # android_push_notification(product_owner_reg_id,message,title,body) if product_owner_reg_id
        product_owner_android_reg_id = product.user.user_regids.where("user_regids.device_os='android'").pluck(:reg_id).uniq
        product_owner_ios_reg_id = product.user.user_regids.where("user_regids.device_os='ios'").pluck(:reg_id).uniq
        android_push_notification(product_owner_android_reg_id,message,title,message) if !product_owner_android_reg_id.empty?
        ios_push_notification(product_owner_ios_reg_id,message,title,message) if !product_owner_ios_reg_id.empty?

        #Push Notification Product owner end
	end


    #To send push notification to product owner for order return
	def send_return_notification(product,message)
        #Push Notification Product owner start
        order_placed_by = find_by_user(product.user_id)
        title = "Order Return Request"
        body = product.headline #+"Order by "+ order_placed_by.name || order_placed_by.email
        # product_owner_reg_id = product.user.user_regids.pluck(:reg_id).uniq!.compact!
        # android_push_notification(product_owner_reg_id,message,title,body) if product_owner_reg_id
        
        product_owner_android_reg_id = product.user.user_regids.where("user_regids.device_os='android'").pluck(:reg_id).uniq.compact
        product_owner_ios_reg_id = product.user.user_regids.where("user_regids.device_os='ios'").pluck(:reg_id).uniq.compact
        android_push_notification(product_owner_android_reg_id,message,title,message) if !product_owner_android_reg_id.empty?
        ios_push_notification(product_owner_ios_reg_id,message,title,message) if !product_owner_ios_reg_id.empty?
        #Push Notification Product owner end
	end

	def find_by_user(id)
		User.find_by_id(id)
	end	

	private

    def order_return_params
        params.require(:order).permit!
    end

    def order_rating_params
        params.require(:order).permit!
    end

    def order_params
        params.require(:order).permit(
        	:status,
        	:amount,
        	:payment_status,
        	:payment_type,
            :transcation_id,
        	:item_quality_rating,
        	:shipping_speed_rating,
            :product_id,
            :user_id,
            :product_feedback,
            :is_cashout_requested,
            :sellers_earnings,
            :my_closet_earnings,
            :card_type,
            :card_number,
            :order_no,
            :shipping_address_id,
            :billing_address_id
        )
    end
end
