class Api::V2::SubscriptionsController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :authenticate_api_v1_user!

    # Get all subscriptions
    def index
        begin
            subscription = Subscription.where(:status => true).try(:first)
            user_subscription = current_api_v1_user.user_subscriptions.where(:status => true)
            offers = get_all_offers
            response = Hash.new
            response[:id] = subscription.try(:id)
            if offers.nil?
                response[:amount] = subscription.amount
                response[:total], response[:payment_charges] = get_amount_with_stripe_charges(response[:amount]) unless subscription.nil?
            else
                offer_price = offers.price
                response[:amount] = offers.price.round(2)
                response[:total], response[:payment_charges] = get_amount_with_stripe_charges(offer_price) unless subscription.nil?
            end
            render success_response_with_object(
                "sucess",
                200,
                {
                    :subscription => response,
                    :renewal_date => !user_subscription.empty? ? (user_subscription.last.created_at + 1.year).strftime("%B %d, %Y") : ""
                }
            )
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    #This method is to create new user subscription
    def create
        params[:subscription][:amount] = (params[:subscription][:amount]/100)
        begin
            subscription = current_api_v1_user.user_subscriptions.create(
                subscription_params
            )
            render success_response_with_object("sucess",200,subscription)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    #This method is to fetch user subscription
    def show      
        begin
            subscriptions = current_api_v1_user.user_subscriptions.where(:status => true)
            render success_response_with_object("sucess",200,subscriptions)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    private
    def subscription_params
        params.require(:subscription).permit!
    end

end
