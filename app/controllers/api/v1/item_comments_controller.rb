class Api::V1::ItemCommentsController < ApplicationController
	include DeviseTokenAuth::Concerns::SetUserByToken
	before_action :authenticate_api_v1_user!

	#This method is to create new message to product owner
	def create		
		begin
			comment = ItemComment.create(item_comment_params)
			sender = User.find_by_id(params[:item_comment][:user_id])
            if current_api_v1_user.id != comment.product.user.try(:id)               
                sender_name = sender.name || sender.uid
                product = comment.product 
	            @order_message = "#{sender_name} posted a comment on #{product.headline}"
	            update_notification('product', product, sender, @order_message)  if product and sender
	            send_comments_notification(product,@order_message)
            end            
			render success_response_with_object("sucess",200,comment)
		rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
	end

	#To send push notification to product owner for wishing product by users
    def send_comments_notification(product,message)
        #Notification.create(private_message_params)
        #Push Notification Product owner start
        order_placed_by = product.user.try(:id)
        title = "New Comment"
        message = message
        body = product.headline #+"Order by "+ order_placed_by.name || order_placed_by.email
        # product_owner_reg_id = product.user.user_regids.pluck(:reg_id).uniq!
        # android_push_notification(product_owner_reg_id,message,title,body) if product_owner_reg_id
        product_owner_android_reg_id = product.user.user_regids.where("user_regids.device_os='android'").pluck(:reg_id).uniq.compact
        product_owner_ios_reg_id = product.user.user_regids.where("user_regids.device_os='ios'").pluck(:reg_id).uniq.compact
        android_push_notification(product_owner_android_reg_id,message,title,body) if !product_owner_android_reg_id.empty?
        ios_push_notification(product_owner_ios_reg_id,message,title,body) if !product_owner_ios_reg_id.empty?
        #Push Notification Product owner end
    end

	private

	def item_comment_params
		params.require(:item_comment).permit(
			:description,
			:user_id,
			:product_id
			)
	end
end
