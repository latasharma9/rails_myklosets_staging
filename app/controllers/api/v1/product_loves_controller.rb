=begin
This module will be used for manipulating myloves and it will get the requests 
from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Sep 27,2016
@author : TAG
=end

class Api::V1::ProductLovesController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :authenticate_api_v1_user!

    def initialize
        @limit = 20
    end

    # This method used for fetch wishlists and its products
    def index
        data = new_hash
        data_array = []
        params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
        user_wishlists = current_api_v1_user.wishlists.order('created_at DESC').limit(@limit).offset(offset_count)
        if user_wishlists.present?
            user_wishlists.includes(:product=>[:attachments,:wishlists]).each do |wish|
                data_array.append(get_my_love_product_details wish.product)
            end
            render success_response_with_object("sucess", 200, data_array)
        else
            render error_response_with_object("No products loved", 400, [])
        end
    end

    # This method used for create a mylove
    def create
        begin
            wish = Wishlist.find_or_create_by(wishlist_params)
            sender = User.find_by_id(current_api_v1_user.id)
            if current_api_v1_user.id != wish.product.user.try(:id)
                sender_name = sender.name || sender.uid
                product = wish.product 
                @order_message = "#{sender_name} loved your listing #{product.headline}"
                update_notification('product', product, sender, @order_message)  if product and sender
                send_wishlist_notification(product,@order_message)
            end            
            render success_response_with_object("sucess", 200, wish)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    #To send push notification to product owner for wishing product by users
    def send_wishlist_notification(product,message)
        #Notification.create(private_message_params)
        #Push Notification Product owner start
        order_placed_by = find_by_user(params[:order][:user_id])
        title = "New Likes"
        message = message
        body = product.headline #+"Order by "+ order_placed_by.name || order_placed_by.email
        product_owner_reg_id = product.user.user_regids.pluck(:reg_id).uniq!
        android_push_notification(product_owner_reg_id,message,title,body) if product_owner_reg_id
        #Push Notification Product owner end
    end


    def get_my_love_product_details(product)
        data_hash = new_hash
        data_hash["id"] = product.id
        data_hash["headline"] = product.headline
        data_hash["selling_price"] = product.selling_price
        data_hash["original_price"] = product.original_price
        data_hash["total"] = product.total
        data_hash["is_sold"] = product.is_sold
        data_hash["attachments"] = \
                        product.attachments.present? ? {:url => 'http:'+product.attachments.first.attachment.url(:small),
                            :caption => product.attachments.first.attachment.try(:caption).try(:title) || ''} : ''
        data_hash["wishlists"] = product.wishlists.length
        data_hash["is_loved"] = true
        return data_hash
    end


    # This method used for delete a wishlist
    def destroy
        begin
            wishlist = Wishlist.find params[:id]
            wishlist.delete
            render success_response("sucess", 200)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    private
    def wishlist_params
        params.permit(
            :product_id,
            :user_id,
        )
    end

end
