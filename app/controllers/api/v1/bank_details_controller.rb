class Api::V1::BankDetailsController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :authenticate_api_v1_user!

    # This method used for create user(seller) bank details
    def create
        begin
            bank_detail = current_api_v1_user.seller_bank_detail
            if bank_detail.nil?
                p params
                params[:bank_details][:user_id] = current_api_v1_user.id
                bank_detail = SellerBankDetail.create(
                    bank_details_params
                )
            else
                bank_detail.update_attributes(bank_details_params)
            end
            render success_response_with_object("sucess", 200, bank_detail)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for get user(seller) bank details
    def show
        begin
            bank_detail = SellerBankDetail.find_by_user_id(current_api_v1_user.id)
            render success_response_with_object("sucess", 200, bank_detail)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    private
    def bank_details_params
        params.require(:bank_details).permit!
    end
end
