class Api::V3::DonationBagsController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
	before_action :authenticate_api_v1_user!

    # This method used for create a new donation bag
    def index
        begin
            donate = DonationBag.find_by_user_id_and_status(current_api_v1_user.id, false)
            render success_response_with_object("sucess", 200,
                {:is_request_processed=>donate.try(:status),
                   :shipping_addresses => current_api_v1_user.shipping_addresses})
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

	# This method used for create a new donation bag
    def create
    	begin
            donation_bag = DonationBag.find_by_user_id_and_status(params[:donation_bag][:user_id], false)
            unless donation_bag.present?
                donate = DonationBag.create(donation_bag_params)
            else
                donate = {"message": 'Already requested'}
            end
            render success_response_with_object("Donation bag request was successful", 200, donate)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

     private
    def donation_bag_params
        params.require(:donation_bag).permit(
            :is_receive_email_receipt,
            :shipping_address,
            :user_id,
            :bags_count
        )
    end
end
