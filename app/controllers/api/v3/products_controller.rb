=begin
This module will be used for manipulating products and it will get the requests 
from client side(Could be mobile devices such as iphone, android, ipad).

Created On : Sep 26,2016
@author : TAG
=end

class Api::V3::ProductsController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :authenticate_api_v1_user!

    def initialize
        @limit = 20
    end

    # This method used for list the all products
    def index
        begin
            params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
            products = Product.where("status=true and is_deleted=false").includes(:attachments, :wishlists, :product_image).order("is_sold ASC").order("created_at DESC").limit(@limit).offset(offset_count)
            data_array = Product.prepare_objects(products,current_api_v1_user)
            render success_response_with_object("sucess", 200, data_array)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for create a new product
    def create
        begin
            if params['file']   
                product = Product.find(params[:id])
                attachment = product.attachments.create(:attachment=>params['file'])
                attachment.create_caption(:title => params[:caption]) if params[:caption]
                unless product.product_image.nil?
                  product.product_image.update_attributes(:attachment_id => attachment.id) if params[:primary_image] && params[:primary_image] == "true"
                else
                  product.create_product_image(:attachment_id => attachment.id) if params[:primary_image] && params[:primary_image] == "true"
                end
                render success_response_with_object("sucess", 200, product)
            else
                product = Product.create(product_params)
                unless product.errors.present?
                    update_product_colors(
                        params[:product_colors],
                        product.id
                    ) if params[:product_colors].present?
                    comments, attachment_urls, product_details, color_codes = \
                        get_product_details(product)
                    render success_response_with_object_product(
                        "sucess",
                        200,
                        product_details,
                        attachment_urls,
                        color_codes,
                        product.wishlists.empty? ? 0 : product.wishlists.length,
                        comments,
                        product.wishlists.empty? ? 0 : product.wishlists.find_by_user_id(current_api_v1_user.id).try(:id) | 0
                    )
                else
                    render error_response(product.errors.messages, 400)
                end
            end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for fetch the product details
    def show
        begin
            product = Product.find params[:id]
            comments, attachment_urls, product_details, color_codes = \
                    get_product_details(product)
            render success_response_with_object_product(
                "sucess",
                200,
                product_details,
                attachment_urls,
                color_codes,
                product.wishlists.empty? ? 0 : product.wishlists.length,
                comments,
                current_api_v1_user.wishlists.empty? ? 0 : (current_api_v1_user.wishlists.find_by_product_id(product.id).try(:id) || 0)
            )
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for update the product details
    def update
        begin
            product = Product.find params[:id]
            product.update_attributes(product_params)            
            if product.update_attributes(product_params)
                update_product_colors(params[:product_colors], product.id) if params[:product_colors]
                #product.attachments.create(:attachment=>params['file']) if params['file']
                comments, attachment_urls, product_details, color_codes = \
                    get_product_details(product)
                render success_response_with_object_product(
                    "sucess",
                    200,
                    product_details,
                    attachment_urls,
                    color_codes,
                    product.wishlists.empty? ? 0 : product.wishlists.length,
                    comments,
                    current_api_v1_user.wishlists.empty? ? 0 : (current_api_v1_user.wishlists.find_by_product_id(product.id).try(:id) || 0)
                )
            else
                render error_response(product.errors.messages, 400)
            end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for delete a product
    def destroy
        begin
            product = Product.find params[:id]
            #product.delete
            product.update_attributes(:is_deleted => true)
            render success_response("sucess", 200)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    #This s the method to delete prodcut attachment with params
    def delete_attachment
        begin
            product =Product.find params[:product_id]
            attachment = product.attachments.where(:id => params[:id])
            attachment.destroy(attachment.first.try(:id))
            attachment_urls = []
            if product.attachments.present?
                product.attachments.each do |attach|
                    attachment_urls << attachment_obj(attach)
                end
            end
            render success_response_with_object("sucess", 200,attachment_urls)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method handles the update product colors
    def update_product_colors colors_id, product_id
        colors = ColorProduct.where(:product_id => product_id)
        if colors.present?
            colors.delete_all
        end
        colors_id.each do |c_id|
            ColorProduct.create(
                :product_id=>product_id,
                :color_id=>c_id
            )
        end
    end

    def get_product_details product
        item_comments = product.item_comments.reverse
        comments = []
        item_comments.each do |ic|
            comments << {:description => ic.description, :created_at =>ic.created_at, :username => ic.user.name || ic.user.uid, :user_image_url=>ic.try(:user).try(:attachment).nil? ? "" : 'http:' + ic.user.attachment.attachment.url(:small)}
        end
        product_user_visit = ProductUserVisit.find_or_create_by(product_id:product.id,user_id:current_api_v1_user.id)
        product_user_visit.update_attributes(:visits=>product_user_visit.visits+1)
        attachment_urls = []
        primary_image = product.product_image       
        if product.attachments.present?
            product.attachments.each do |attachment|
                if primary_image && primary_image.attachment_id == attachment.id
                    attachment_urls.unshift(attachment_obj(attachment))
               else
                    attachment_urls << attachment_obj(attachment)
               end
            end
        end
        product_details = {
            :id => product.id,
            :product_user_id => product.user.id,
            :product_user_name => product.try(:user).try(:name) || product.user.email,
            :headline => product.headline,
            :description => product.description,
            :selling_price => product.selling_price,
            :original_price => product.original_price,
            :total => product.total,
            :size => product.try(:size).try(:bust) || '',
            :shirt_size => product.try(:shirt_size).try(:bust) || '',
            :cup => product.try(:cup).try(:name) || '',
            :condition => product.try(:condition).try(:name) || '',
            :fabric => product.try(:fabric).try(:name) || '',
            :type => product.try(:type).try(:name) || '',
            :status => product.status,
            :is_sold => product.is_sold,
            :is_rent => product.is_rent,
            :is_donated => product.is_donated,
            :is_subscribed_user => current_api_v1_user.is_user_subscribed
        }
        color_codes = []
        ColorProduct.where(:product_id => product.id).each do |color_product|
            color_codes << color_product.try(:color).try(:code) || ''
        end
        return comments, attachment_urls, product_details, color_codes
    end

    def attachment_obj(attachment)
        obj = {:url => 'http:' + attachment.attachment.url,
         :caption => attachment.try(:caption).try(:title) || '',
         :id => attachment.id
        }
    end



    # This method handles mycloset list with lovecount
    def my_closet
        begin
            filtered_data = filter_sort_product_details(
                current_api_v1_user.products.where(:is_deleted => false).by_latest_order.includes(
                    :attachments, :wishlists),
                current_api_v1_user
            )
            render success_response_with_object("sucess", 200, filtered_data)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end


    # This method handles mycloset list with lovecount
    def other_user_closet
        begin
            other_user = User.find(params[:id])
            filtered_data = filter_sort_product_details(
                other_user.products.by_latest_order.includes(
                    :attachments, :wishlists
                ).where(:is_deleted => false),
                other_user
            )
            render success_response_with_object("sucess", 200, filtered_data)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    #This is method used for set primary product image to display user
    def set_primary_product_image
        begin
            product_image = ProductImage.find_by_product_id(params[:id])            
            unless product_image.nil?
              product_image.update_attributes(product_image_params)
            else
               ProductImage.create(product_image_params) 
            end
            render success_response_with_object("sucess", 200, {"success" => true})
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for filter the products
    def filters
        begin
            if params['filter'].has_key?('selling_price')
                if params["filter"]["selling_price"].include? "-"
                    params["filter"]["selling_price"] = params["filter"]["selling_price"].split('-')[0].to_f..params["filter"]["selling_price"].split('-')[-1].to_f
                else
                    params["filter"]["selling_price"] = params["filter"]["selling_price"].to_f..Float::INFINITY
                end
            end
            if params['filter'].has_key?('color_id')
                params['filter']['id'] = ColorProduct.where(
                    'color_id' => params['filter']['color_id']
                ).pluck('product_id')
            end
            params[:filter].delete(:color_id)
            params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
            if params.has_key?('sort_by')
                products = Product.where(filter_params).where(:is_deleted =>false, :status => true).includes(:product_image).order("is_sold ASC").get_sorted_products(params[:sort_by]).limit(@limit).offset(offset_count)
            else
                products = Product.where(filter_params).where(:is_deleted =>false, :status => true).includes(:product_image).order("is_sold ASC").order("created_at DESC").includes(
                    :attachments, :wishlists
                ).limit(@limit).offset(offset_count)
            end
            filtered_data = filter_sort_product_details(products, current_api_v1_user)
            render success_response_with_object("sucess", 200, filtered_data)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # This method used for perform sorting operation
    def sorting
         begin
            params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
            # Sorting with filter           

            if params.has_key?('filter')
                if params['filter'].has_key?('selling_price')
                    if params["filter"]["selling_price"].include? "-"
                        params["filter"]["selling_price"] = params["filter"]["selling_price"].split('-')[0].to_f..params["filter"]["selling_price"].split('-')[-1].to_f
                    else
                        params["filter"]["selling_price"] = params["filter"]["selling_price"].to_f..Float::INFINITY
                    end
                end                    
                if params['filter'].has_key?('color_id')
                    params['filter']['id'] = ColorProduct.where(
                        'color_id' => params['filter']['color_id']
                    ).pluck('product_id')
                end

                params[:filter].delete(:color_id)

                products = Product.where(filter_params).where(:is_deleted =>false, :status => true).order("is_sold ASC").includes(:product_image).get_sorted_products(params[:sort_by]).limit(@limit).offset(offset_count)
            else
                products = Product.where(:is_deleted => false, :status => true).order("is_sold ASC").includes(:product_image).get_sorted_products(params[:sort_by]).limit(@limit).offset(offset_count)
            end
            unless products == 'Invalid'
                sorted_data = filter_sort_product_details(products, current_api_v1_user)
                render success_response_with_object("sucess", 200, sorted_data)
            else
                render error_response('Invalid params', 400)
            end
         rescue Exception => e
             render error_response_with_object("Error", 500, e.message)
         end
    end

    # This method used for perform search
    def search_by_query
        begin
            products = Product.where("is_deleted=false").search_by_text(params[:search_query])
            search_data = filter_sort_product_details(products, current_api_v1_user)
            render success_response_with_object("sucess", 200, search_data)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def filter_sort_product_details products, user
        Product.prepare_objects(products, user)
    end

    def purchase_confirmation
        begin
            product = Product.find_by_id(params[:product_id])
            product_owner = product.user        
            buyer = User.find_by_id(params[:user_id])
            #Get total amount for payment and stripe charges
            # total, stripe_charges = get_amount_with_stripe_charges(product.selling_price) 
            if product.total.nil?
                total, stripe_charges = get_amount_with_stripe_charges(product.selling_price) 
            else
                total = product.total.round(2)
            end
            purchase_details = {
                product_details: {
                    id: product.id,
                    is_sold: product.is_sold,
                    headline: product.headline,
                    selling_price: product.selling_price,
                    is_rent: product.is_rent,
                    attachment: product.attachments.present? ? "http:"+product.attachments.first.attachment.url(:medium) : "img/img_product-3.jpg"
                },
                shipping_details: [],
                shipping_addresses: buyer.shipping_addresses,
                billing_addresses: buyer.billing_addresses,
                stripe_id: buyer.stripe_id,
                charges: {
                    payment_charges:stripe_charges.round(2),
                    total:total.round(2)
                }
            }
            render success_response_with_object("sucess",200,purchase_details)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end

    end


    private

    def filter_params
        params.require(:filter).permit!
    end

    def product_params
        params.require(:product).permit!
    end

    def product_image_params
        params.require(:product_image).permit(:attachment_id, :product_id)
    end
end
