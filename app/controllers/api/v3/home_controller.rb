class Api::V3::HomeController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    include Devise::Controllers::Helpers

    before_action :authenticate_api_v1_user!, :except=>['send_reset_password','reset_password']

    # This method used for get all products
    def index
        #new_arrivals = Product.last(9)
        categories = Type.select('id', 'name')
        data_array = []
  
        user_subscriptions = UserSubscription.where(:status => true).order('created_at DESC').includes(:user=>[:attachment,:products])
        user_subscriptions.each do |u_sub|
            products = u_sub.user.products.find_all{|prod| prod.status==true and prod.is_deleted==false and prod.is_sold==false}
            if products.length > 0
                if data_array.any? {|h| h['user_id'] == u_sub.user_id }
                    obj = data_array.select {|h| h['user_id'] == u_sub.user_id }
                    obj[0]["count"] = obj[0]["count"] + products.length
                else
                    # unless current_api_v1_user.id == u_sub.user_id 
                        h = new_hash
                        h["user_id"] = u_sub.user_id
                        h["user_name"] = u_sub.user.name || u_sub.user.uid                    
                        h['user_image_url'] = u_sub.user.attachment.nil? ? "" : 'http:' + u_sub.user.attachment.attachment.url(:small)
                        h["count"] = products.length               
                        data_array.append(h)               
                    # end
                end
            end
        end
        featured_sellers =  data_array.sort_by { |da| da['count'] }.reverse
        render success_response_with_object(
            "sucess",
            200,
            {
                :categories => categories,
                :featured_sellers => featured_sellers.shuffle.sample(4),
                :private_messsage_count => User.private_messages_count(current_api_v1_user),
                :notification_count => current_api_v1_user.notifications_count,
                :is_subscribed_user => current_api_v1_user.is_user_subscribed,
                :user_image_url => current_api_v1_user.attachment.nil? ? "" : 'http:' + current_api_v1_user.attachment.attachment.url(:small),
                :offer => get_all_offers
            }
        )

    end

    def get_subscription_and_charges
        render success_response_with_object(
            "sucess",
            200,
            {
                :is_subscribed_user => current_api_v1_user.is_user_subscribed,
                :stripe_add_cents => CONFIG["stripe_charges_additional_cent"],
                :stripe_percentage_charges => CONFIG["stripe_charges_in_percent"],
                :mycloset_percentage_charges => CONFIG["listing_fee"]
            }
        )
    end

    # Get default values
    def get_default_values
        begin
            render success_response_with_object(
                "sucess",
                200,
                {:types => Type.select('id', 'name'),
                    :fabrics => Fabric.select('id', 'name'),
                    :conditions => Condition.select('id', 'name'),
                    :cups => Cup.select('id', 'name'),
                    :size => Size.select('id', 'bust'),
                    :shirt_size => ShirtSize.select('id', 'bust'),
                    :colors => Color.select('id', 'name', 'code'),
                    :is_subscribed_user => current_api_v1_user.is_user_subscribed,
                    :stripe_add_cents => CONFIG["stripe_charges_additional_cent"],
                    :stripe_percentage_charges => CONFIG["stripe_charges_in_percent"],
                    :mycloset_percentage_charges => CONFIG["listing_fee"]
                }
            )
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end


    # get product type details
    def get_types
        begin
            render success_response_with_object("sucess", 200, Type.all)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # get product fabric details
    def get_fabrics
        begin
            render success_response_with_object("sucess", 200, Fabric.all)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # get product condition details
    def get_conditions
        begin
            render success_response_with_object("sucess", 200, Condition.all)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # get product cup details
    def get_cups
        begin
            render success_response_with_object("sucess", 200, Cup.all)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # get product size details
    def get_size
        begin
            render success_response_with_object("sucess", 200, Size.all)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    # get product size details
    def get_colors
        begin
            render success_response_with_object("sucess", 200, Color.all)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def gcm_register
        begin
            existing_user_reg_id = UserRegid.find_by_reg_id(params[:reg_id])
            if existing_user_reg_id.nil?
                register = UserRegid.find_or_create_by(gcm_register_params)
            else
                unless existing_user_reg_id.user_id == current_api_v1_user.id                  
                  existing_user_reg_id.update_attributes(:user_id => params[:user_id])
                end
            end
            is_subscribed_user = current_api_v1_user.is_user_subscribed
            #android_push_notification([register.reg_id],"My first push notification!","Notification title","Welcome to mycloset!") if register
            render success_response_with_object("sucess", 200,{:is_subscribed_user => is_subscribed_user})
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def send_reset_password
        # begin
            user = User.find_by_email(params[:email])
            unless user.nil? 
                if user.provider == "email"
                    UserMailer.reset_password_instructions(user).deliver
                    render success_response("sucess", 200)
                else
                   render error_response("Email is not found!", 400, )  
                end
            else
               render error_response("Email is not found!", 400, )   
            end            
        # rescue Exception => e
        #     render error_response_with_object("Error", 500, e.message)
        # end
    end


    def reset_password
        begin
            user = User.find_by_reset_password_token(params[:token])
            if user.present?
                if params.has_key?('password') && params.has_key?('password_confirmation')
                    if user.update(user_params)
                        render success_response_with_object(
                            "sucess",
                            200,
                            {"id": user.id}
                        )
                    else
                        error = user.errors.messages.has_key?(:password) ? "Password "+user.errors.messages[:password][0]: "Something went wrong!"
                        render error_response_with_object(error, 500, user.errors)
                    end
                else
                    render success_response_with_object(
                        "sucess",
                        200,
                        {'token': params[:token]}
                    )
                end
            else
                render error_response("Invalid code.", 400)
            end
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end

    def update_stripe
        begin
            user = User.find_by_id(params[:user_id])
            unless user.nil? 
                user.update_attributes(:stripe_id => params[:stripe_id])
                render success_response_with_object(
                    "sucess",
                    200,
                    user
                )
            else
               render error_response("User not found!", 400, )   
            end            
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
    end


    protected
    def user_params
        params.permit(:password, :password_confirmation)
    end

    def gcm_register_params
        params.permit(:reg_id, :device_os, :user_id)
    end

end
