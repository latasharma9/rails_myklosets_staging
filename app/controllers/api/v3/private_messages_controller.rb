class Api::V3::PrivateMessagesController < ApplicationController
	include DeviseTokenAuth::Concerns::SetUserByToken
	before_action :authenticate_api_v1_user!

	def initialize
		@limit = 20
	end

	#This method is to create new message to product owner
	def create		
		begin
			message = PrivateMessage.create(private_message_params)
			message_object = PrivateMessage.prepare_message_object(message)			
			render success_response_with_object("sucess",200,message_object)
		rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
	end


	#This method is to get private messages by user
	def index
		params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
		begin
			messages = PrivateMessage.where("receiver_id = ? OR user_id = ?", current_api_v1_user.id, current_api_v1_user.id).order("created_at DESC").limit(@limit).offset(offset_count)
			messages_res = []
			messages.each do |m|
				# unless m.user.attachment.nil?
    #                 user_image_url = 'http:' + m.user.attachment.attachment.url(:small)
    #             else
    #                 user_image_url = ""
    #             end
				messages_res << { :id => m.id, :message => m.message, :username => m.user.name || m.user.uid, :user_id =>m.user.id, :user_image_url => m.user.attachment.nil? ? "" : ('http:' + m.user.attachment.attachment.url(:small)), :created_at => m.created_at}
			end
			messages.update_all(:is_read => true)
			render success_response_with_object("sucess",200,messages_res)
		rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end		
	end

	def show
		params.has_key? (:offset) ? offset_count = params[:offset].to_i : offset_count = 0
		begin
			message = PrivateMessage.find_by_id(params[:id])
			receiver_id = message.user.id
			if message.parent_id.nil?
				parent_message = message				
			else
				parent_message = PrivateMessage.find_by_id(message.parent_id)
			end
			sub_messages = PrivateMessage.where(:parent_id => parent_message.id).order("created_at ASC").limit(@limit).offset(offset_count)
			messages = PrivateMessage.prepare_message_conversation(parent_message,sub_messages,receiver_id)
			render success_response_with_object("sucess",200,messages)
		rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end
	end

	private

	def private_message_params
		params.require(:private_message).permit(
			:message,
			:is_read,
			:user_id,
			:receiver_id,
			:product_id,
			:parent_id
			)
	end
end
