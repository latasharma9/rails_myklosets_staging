class Api::V3::CashoutsController < ApplicationController
    include DeviseTokenAuth::Concerns::SetUserByToken
    before_action :authenticate_api_v1_user!

    # This method used for create cashout details
    def create
        begin
            cashout = current_api_v1_user.cashouts.create(cashout_params)
            user_orders = current_api_v1_user.orders.where(:id => params[:cashout][:order_ids])
            user_orders.update_all(
                    :is_cashout_requested => true,
                    :cashout_id => cashout.id
            )
            render success_response_with_object("sucess", 200, cashout)
        rescue Exception => e
            render error_response_with_object("Error", 500, e.message)
        end


    end

    private
    def cashout_params
        params.require(:cashout).permit!
    end

end
